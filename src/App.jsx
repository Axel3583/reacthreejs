import { Canvas } from '@react-three/fiber'
import Box from './Box'

export default function App() {
  return (
    <Canvas>
     <Box position={[-0.1, 0, 2]} name="A" />
     <Box position={[0.7, 2, 0]} name="B" />
    </Canvas>
  )
}